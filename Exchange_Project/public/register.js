const humanize = (str) => {
  let i,
    frags = str.split("_");
  for (i = 0; i < frags.length; i++) {
    frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
  }
  return frags.join(" ").replace("And", "&");
};
const categorySection = document.querySelector(".category-section");
const fetchData = async () => {
  const categoryDataJSON = await fetch("/category");
  const categoryData = await categoryDataJSON.json();
  let optionHTML = "";
  for (let c = 0; c < categoryData.length; c++) {
    optionHTML += `<a class="dropdown-item" href="/search?cate=${
      categoryData[c].id
    }">${humanize(categoryData[c].name)}</a>`;
  }
  categorySection.innerHTML = optionHTML;
};

fetchData();

window.onload = function () {
  const searchParams = new URLSearchParams(window.location.search);
  const errMessage = searchParams.get("error");

  if (errMessage) {
    const alertBox = document.createElement("div");
    alertBox.classList.add("alert", "alert-danger");
    alertBox.textContent = errMessage;
    document.querySelector("#error-message").appendChild(alertBox);
  } else {
    document.querySelector("#error-message").innerHTML = "";
  }
};
