import express from "express";
import path from "path";
import { knex } from "./server";

export const transactionRecordRoute = express.Router();

export const renderTransactionRecordPage = (
  req: express.Request,
  res: express.Response
) => {
  res.sendFile(
    path.resolve(
      __dirname,
      "protected/exchange-process/transactionRecord/transactionRecord.html"
    )
  );
};

export const getTransactionRecord = async (
  req: express.Request,
  res: express.Response
) => {
  const record = await knex.select("*").from("transaction_record").where("id", req.params.record_id)
  const targetUser = await knex.select(["id", "username", "profilepicture"]).from("users").where("id", record[0].target_user_id)
  const RequestUser = await knex.select(["id", "username", "profilepicture"]).from("users").where("id", record[0].visitor_user_id)

  if (
    record[0].target_user_id == req.params.user_id ||
    record[0].visitor_user_id == req.params.user_id
  ) {
    res.json({ record, targetUser, RequestUser });
  } else {
    res.json({ result: "none" });
  }
};

transactionRecordRoute.get("/", renderTransactionRecordPage);
transactionRecordRoute.get(
  "/get-record/:record_id/:user_id",
  getTransactionRecord
);
