import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("category").del();

    // Inserts seed entries
    await knex("category").insert([
        { name: "home_and_furniture"},
        { name: "electroncs"},
        { name: "women_fashion"},
        { name: "men_fashion"},
        { name: "toy_and_games"},
        { name: "kids_clothes"},
        { name: "kitchen_and_appliances"},
        { name: "books"},
        { name: "sports"},
        { name: "others"},
    ]);
};
