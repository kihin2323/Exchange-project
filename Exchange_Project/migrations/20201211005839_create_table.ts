import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable("users")){
        return;
    }
    await knex.schema.createTable("users", (table)=>{
        table.increments();
        table.string("username").notNullable();
        table.string("email").notNullable();
        table.string("password").notNullable();
        table.string("profilepicture");
        table.boolean("is_google");
        table.timestamps(false, true);
    })
    if (await knex.schema.hasTable("category")){
        return;
    }
    await knex.schema.createTable("category", (table)=>{
        table.increments();
        table.string("name");
    })
    if (await knex.schema.hasTable("product")){
        return;
    }
    await knex.schema.createTable("product", (table)=>{
        table.increments();
        table.integer("category_id").notNullable();
        table.foreign("category_id").references("category.id");
        table.integer("user_id");
        table.foreign("user_id").references("users.id");
        table.string("product_name").notNullable();
        table.timestamps(false, true);
        table.string("location");
        table.text("description");
        table.integer("view");
        table.text("wished_goods_to_exchange");
        table.boolean("accepted_for_exchange");
    })

    if (await knex.schema.hasTable("comment")){
        return;
    }
    await knex.schema.createTable("comment", (table)=>{
        table.increments();
        table.integer("user_id");
        table.foreign("user_id").references("users.id");
        table.integer("product_id");
        table.foreign("product_id").references("product.id");
        table.text("content");
        table.timestamps(false, true);

    })
    if (await knex.schema.hasTable("rating")){
        return;
    }
    await knex.schema.createTable("rating", (table)=>{
        table.increments();
        table.integer("target_user_id");
        table.foreign("target_user_id").references("users.id");
        table.integer("visitor_id");
        table.foreign("visitor_id").references("users.id");
        table.integer("rating")
    })
    if (await knex.schema.hasTable("product_image")){
        return;
    }
    await knex.schema.createTable("product_image", (table)=>{
        table.increments();
        table.integer("product_id");
        table.foreign("product_id").references("product.id");
        table.string("product_image")
    })
    if (await knex.schema.hasTable("like")){
        return;
    }
    await knex.schema.createTable("like", (table)=>{
        table.increments();
        table.integer("user_id");
        table.foreign("user_id").references("users.id");
        table.integer("product_id");
        table.foreign("product_id").references("product.id");
    })
    if (await knex.schema.hasTable("exchange_product_proposal")){
        return;
    }
    await knex.schema.createTable("exchange_product_proposal", (table)=>{
        table.increments();
        table.integer("target_product_id");
        table.foreign("target_product_id").references("product.id");
        table.integer("user_id").notNullable();
        table.foreign("user_id").references("users.id");
        table.integer("visitor_product_id");
        table.foreign("visitor_product_id").references("product.id");
        table.integer("target_product_user_id");
        table.foreign("target_product_user_id").references("users.id");
        table.boolean("reject")
        table.boolean("accept")
        table.boolean("withdraw")
    })
    if (await knex.schema.hasTable("suggested_time")){
        return;
    }
    await knex.schema.createTable("suggested_time", (table)=>{
        table.increments();
        table.integer("exchange_product_proposal_id");
        table.foreign("exchange_product_proposal_id").references("exchange_product_proposal.id");
        table.string("from_target_user")
        table.string("from_requester")
        table.string("content")
        table.string("date")
        table.string("time_from")
        table.string("time_to")
        table.string("location")
        table.boolean("accept")
    })
    if (await knex.schema.hasTable("transaction_record")){
        return;
    }
    await knex.schema.createTable("transaction_record", (table)=>{
        table.increments();
        table.integer("target_user_id").notNullable();
        table.foreign("target_user_id").references("users.id");
        table.integer("visitor_user_id").notNullable();
        table.foreign("visitor_user_id").references("users.id");
        table.boolean("confirm_from_target_user")
        table.boolean("confirm_from_requester")
        table.string("target_product_name").notNullable()
        table.string("target_product_image")
        table.string("visitor_product_name").notNullable()
        table.string("visitor_product_image")
        table.string("content")
        table.string("date").notNullable()
        table.string("time_from")
        table.string("time_to")
        table.string("location").notNullable()
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("users");
    await knex.schema.dropTableIfExists("category");
    await knex.schema.dropTableIfExists("product");
    await knex.schema.dropTableIfExists("comment");
    await knex.schema.dropTableIfExists("rating");
    await knex.schema.dropTableIfExists("product_image");
    await knex.schema.dropTableIfExists("like");
    await knex.schema.dropTableIfExists("exchange_product_proposal");
    await knex.schema.dropTableIfExists("suggested_time");
    await knex.schema.dropTableIfExists("transaction_record");
}

