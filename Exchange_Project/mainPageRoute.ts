import express from "express";
import path from "path";
import {knex} from './server'

export const mainPageRoute = express.Router();

export const getCategory = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    const category = await knex.select("*").from("category")
  res.json(category);
  }catch(e){
    console.log("getCategory: ", e)
  }
  
};

export const checkUser = async (
  req: express.Request,
  res: express.Response
) => {
  try{

  
  if (req.session && req.session.user) {
    const fetchRequest = await knex.select("*").from("exchange_product_proposal").where("target_product_user_id", req.session.user.id)
    const fetchSendRequest = await knex.select("*").from("exchange_product_proposal").where("user_id", req.session.user.id)
    const fetchedData = await knex.select(["id", "username", "profilepicture", "created_at", "email"]).from("users").where("id", req.session.user.id)
    res.json({ fetchedData, fetchRequest, fetchSendRequest, result: "found" });
  } else {
    res.json({ result: "none" });
  }
  }catch(e){
    console.log("checkUser: ", e)
  }
};
export const renderMainPage = (req: express.Request, res: express.Response) => {
  res.sendFile(path.resolve(__dirname, "./public/mainPage.html"));
};

export const storeComment = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    if (req.session) {
      await knex.insert({user_id: req.session.user.id,product_id: req.params.productId, content: req.body.comment}).into("comment")
    }
    res.redirect("/profile/detail?id=" + req.params.productId);
  }catch(e){
    console.log("storeComment: ", e)
  }
};

export const checkComment = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    const userArr: any[] = [];
    const result = await knex.select(["id", "user_id", "content"]).from("comment").where("product_id", req.params.productId)
  for (let i = 0; i < result.length; i++) {
    const users = await knex.select(["id", "username", "profilepicture"]).from("users").where("id", result[i].user_id)
    userArr.push(users);
  }
  res.json({
    result,
    userArr,
  });
  }catch(e){
    console.log("checkComment: ", e)
  }
  
};

export const checkLike = async (
  req: express.Request,
  res: express.Response
) => {
  try{

    const result = await knex.select("user_id").from("like").where("product_id", req.params.productId)
  res.json({
    result,
  });
}catch(e){
    console.log("checkLike: ", e)
}
};
export const removeLike = async (
  req: express.Request,
  res: express.Response
) => {
  try{

  
  if (req.session) {
    await knex("like").where("product_id", req.params.productId).andWhere("user_id", req.session.user.id).del()
  }
  res.redirect("/profile/detail?id=" + req.params.productId);
}catch(e){
    console.log("removeLike: ", e)
}
};
export const storeLike = async (
  req: express.Request,
  res: express.Response
) => {
  if (req.session) {
    await knex.insert({product_id: req.params.productId,user_id: req.session.user.id}).into("like")
  }
  res.redirect("/profile/detail?id=" + req.params.productId);
};

export const uncomment = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    await knex("comment").where("id", req.params.commentId).del()
  res.redirect("/profile/detail?id=" + req.params.productId);
}catch(e){
    console.log("uncomment: ", e)
}
};

export const searchResult = (req: express.Request, res: express.Response) => {
  res.sendFile(path.resolve(__dirname, "./public/searchResult.html"));
};

export const fetchSketchProductDetail = async (
  req: express.Request,
  res: express.Response
) => {
  try{

    const products = await knex.select(["product.id",
    "product.user_id",
    "product.product_name",
    "product.created_at",
    "product.location",
    "product.description",
    "product.accepted_for_exchange",
    "product.wished_goods_to_exchange",
    "product_image.product_image"]).from("product").innerJoin("product_image", "product.id", "product_image.product_id").where("product.id", req.params.id)
  let groupedProducts = new Map();
  for (let item of products) {
    const image = {
      image: item.product_image,
    };
    if (groupedProducts.has(item.id)) {
      groupedProducts.get(item.id).images.push(image);
    } else {
      const product = {
        product_id: item.id,
        product_user_id: item.user_id,
        product_location: item.location,
        acceptedForExchange: item.accepted_for_exchange,
        description: item.description,
        product_name: item.product_name,
        created_at: item.created_at,
        wishedForExchange: item.wished_goods_to_exchange,
        location: item.location,
        images: [image],
      };
      groupedProducts.set(item.id, product);
    }
  }
  const result = Array.from(groupedProducts.values());
  res.json({ result });
}catch(e){
    console.log("fetchSketchProductDetail: ", e)
}
};
export const fetchProductDetail = async (
  req: express.Request,
  res: express.Response
) => {
  try{

  
  const comment = (
    await knex.raw(`SELECT * FROM "comment" WHERE product_id = ${req.params.id}`)
  ).rows;
  const products = await knex.select(["product.id",
    "product.user_id",
    "product.product_name",
    "product.created_at",
    "product.location",
    "product.description",
    "product.wished_goods_to_exchange",
    "product.accepted_for_exchange",
    "users.username",
    "product_image.product_image"]).from("product").innerJoin("product_image", "product.id", "product_image.product_id").innerJoin("users","product.user_id", "users.id").where("product.id", req.params.id)


  let groupedProducts = new Map();
  for (let item of products) {
    const image = {
      image: item.product_image,
    };
    const user = {
      user: item.username,
    };
    if (groupedProducts.has(item.id)) {
      groupedProducts.get(item.id).images.push(image);
      groupedProducts.get(item.id).usernames.push(user);
    } else {
      const product = {
        product_id: item.id,
        product_user_id: item.user_id,
        product_location: item.location,
        description: item.description,
        product_name: item.product_name,
        created_at: item.created_at,
        wishedForExchange: item.wished_goods_to_exchange,
        acceptedForExchange: item.accepted_for_exchange,
        location: item.location,
        images: [image],
        usernames: [user],
      };
      groupedProducts.set(item.id, product);
    }
  }
  const result = Array.from(groupedProducts.values());
  const user = await knex.select([
  "users.id",
  "users.username",
  "rating.rating"]).from("users").innerJoin("rating", "users.id", "rating.target_user_id").where("users.id", result[0].product_user_id)

  if (req.session && req.session.user) {

    res.json({
      result,
      comment,
      user,
      visitorId: req.session.user.id,
    });
  } else {
    res.json({
      result,
      comment,
      user,
      visitorId: "none",
    });
  }
}catch(e){
    console.log("fetchProductDetail: ", e)
}
};

export const getProductDetail = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    const products = await knex.select([
    "product.id",
    "product.product_name",
    "product.created_at",
    "product.location",
    "product.accepted_for_exchange",
    "product.wished_goods_to_exchange",
    "product_image.product_image"
  ]).from("product").innerJoin("product_image", "product.id", "product_image.product_id").where("product.user_id", req.params.id)
  let groupedProducts = new Map();
  for (let item of products) {
    const image = {
      image: item.product_image,
    };
    if (groupedProducts.has(item.id)) {
      groupedProducts.get(item.id).images.push(image);
    } else {
      const product = {
        product_id: item.id,
        product_name: item.product_name,
        created_at: item.created_at,
        acceptedForExchange: item.accepted_for_exchange,
        wishedForExchange: item.wished_goods_to_exchange,
        location: item.location,
        images: [image],
      };
      groupedProducts.set(item.id, product);
    }
  }
  const result = Array.from(groupedProducts.values());
  res.json({
    result,
  });
}catch(e){
    console.log("getProductDetail: ", e)
}
};

mainPageRoute.get("/check-like/:productId", checkLike);
mainPageRoute.get("/check-comment/:productId", checkComment);
mainPageRoute.get("/", renderMainPage);
mainPageRoute.post("/like/:productId", storeLike);
mainPageRoute.post("/dislike/:productId", removeLike);
mainPageRoute.delete("/comment/delete/:productId/:commentId", uncomment);
mainPageRoute.get("/search", searchResult);
mainPageRoute.get("/detail/product/:id", fetchProductDetail);
mainPageRoute.get("/detail/product/sketch/:id", fetchSketchProductDetail);
mainPageRoute.get("/check-log", checkUser);
mainPageRoute.get("/detail/data/:id", getProductDetail);
mainPageRoute.post("/comment/:productId", storeComment);
mainPageRoute.get("/category", getCategory);
