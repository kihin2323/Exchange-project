window.onload = function () {
  const searchParams = new URLSearchParams(window.location.search);
  const errMessage = searchParams.get("error");

  if (errMessage) {
    const alertBox = document.createElement("div");
    alertBox.classList.add("alert", "alert-danger");
    alertBox.textContent = errMessage;
    document.querySelector("#error-message").appendChild(alertBox);
  } else {
    document.querySelector("#error-message").innerHTML = "";
  }
};

const humanize = (str) => {
  let i,
    frags = str.split("_");
  for (i = 0; i < frags.length; i++) {
    frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
  }
  return frags.join(" ").replace("And", "&");
};
const categorySection = document.querySelector(".category-section");
const profilePreview = document.querySelector(".profile-preview");
const logDiv = document.querySelector(".log-div");
const requestSection = document.querySelector(".request-section");
const notificationForReject = document.querySelector(".notification-reject");
const notificationForWithdraw = document.querySelector(
  ".notification-withdraw"
);
const sendRequest = document.querySelector(".request-condition");
const notificationForCompletedRequest = document.querySelector(
  ".notification-completed-request"
);
const fetchData = async () => {
  const data = await fetch("/check-log");
  const preview = await data.json();
  const categoryDataJSON = await fetch("/category");
  const categoryData = await categoryDataJSON.json();
  let optionHTML = "";
  for (let c = 0; c < categoryData.length; c++) {
    optionHTML += `<a class="dropdown-item" href="/search?cate=${
      categoryData[c].id
    }">${humanize(categoryData[c].name)}</a>`;
  }
  categorySection.innerHTML = optionHTML;

  if (preview.result !== "none") {
    const checkRejectRequest = await fetch("/exchange/check-reject-request");
    const rejectedRequest = await checkRejectRequest.json();
    const checkWithdrawRequest = await fetch(
      "/exchange/check-withdraw-request"
    );
    const withdrewRequest = await checkWithdrawRequest.json();
    const checkCompletedRequestJSON = await fetch("/exchange/check-confirm");
    const checkCompleteRequest = await checkCompletedRequestJSON.json();
    let addressImage = "";
    if (preview.fetchedData[0].profilepicture.slice(0, 5) === "https") {
      addressImage = preview.fetchedData[0].profilepicture;
    } else {
      addressImage = "/" + preview.fetchedData[0].profilepicture;
    }
    profilePreview.innerHTML = `<a href="/profile?id=${preview.fetchedData[0].id}"><div class="image"><img src="${addressImage}" /></div><p class="preview-name">${preview.fetchedData[0].username}</p></a>`;
    logDiv.innerHTML = `<a href="/logout" class="log-out-tag">Logout</a>`;
    if (rejectedRequest.length !== 0) {
      let notificationRejectHTML = `<a class="nav-link dropdown-toggle notification" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Request Rejected <i class="fas exclamation fa-exclamation"></i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let h = 0; h < rejectedRequest.length; h++) {
        notificationRejectHTML += `<div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${rejectedRequest[h][0].product_image}" alt="picture of ${rejectedRequest[h][0].product_name}"/></div><div><p class="product-name-confirm">${rejectedRequest[h][0].product_name}</p>
                <form action="/exchange/confirm-reject-withdraw/${rejectedRequest[h][1]}?_method=DELETE" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form>
                </div></div>`;
      }
      notificationRejectHTML += `</div>`;
      notificationForReject.innerHTML = notificationRejectHTML;
    }
    if (withdrewRequest.length !== 0) {
      let notificationWithdrawHTML = `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Withdrawn Request <i class="fas exclamation fa-exclamation"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let j = 0; j < withdrewRequest.length; j++) {
        console.log();
        notificationWithdrawHTML += `<div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${withdrewRequest[j][0].product_image}" alt="picture of ${withdrewRequest[j][0].product_name}"/></div><div><p class="product-name-confirm">${withdrewRequest[j][0].product_name}</p>
              <form action="/exchange/confirm-reject-withdraw/${withdrewRequest[j][1]}?_method=DELETE" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form>
              </div></div>`;
      }
      notificationWithdrawHTML += `</div>`;
      notificationForWithdraw.innerHTML = notificationWithdrawHTML;
    }

    if (preview.fetchRequest.length !== 0) {
      let navbarRequestHTML = "";
      navbarRequestHTML += `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Exchange Request <i class="fas exclamation fa-exclamation"></i>
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let i = 0; i < preview.fetchRequest.length; i++) {
        if (
          !preview.fetchRequest[i].reject &&
          !preview.fetchRequest[i].withdraw
        ) {
          const requestedProductData = await fetch(
            "/detail/product/sketch/" +
              preview.fetchRequest[i].target_product_id
          );
          const requestedProduct = await requestedProductData.json();
          navbarRequestHTML += `<a class="dropdown-item" href="/exchange/receive?requester_product_id=${preview.fetchRequest[i].visitor_product_id}&target_product_id=${preview.fetchRequest[i].target_product_id}&request_data_id=${preview.fetchRequest[i].id}"><div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${requestedProduct.result[0].images[0].image}" alt="picture of ${requestedProduct.result[0].product_name}" /></div><div class="word-section"><p class="product-name">${requestedProduct.result[0].product_name}</p><i class="fas fa-sign-in-alt click-in-for-more"></i></div></div></a>`;
        }
      }

      navbarRequestHTML += `</div>`;
      requestSection.innerHTML = navbarRequestHTML;
    }
    if (preview.fetchSendRequest.length !== 0) {
      let sendRequestHTML = "";
      sendRequestHTML += `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Sent Request</i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let k = 0; k < preview.fetchSendRequest.length; k++) {
        if (
          !preview.fetchSendRequest[k].reject &&
          !preview.fetchSendRequest[k].withdraw
        ) {
          const requestedProductData = await fetch(
            "/detail/product/sketch/" +
              preview.fetchSendRequest[k].target_product_id
          );
          const requestedProduct = await requestedProductData.json();
          sendRequestHTML += `<a class="dropdown-item" href="/exchange/to-exchange?requester_product_id=${preview.fetchSendRequest[k].visitor_product_id}&target_product_id=${preview.fetchSendRequest[k].target_product_id}&request_data_id=${preview.fetchSendRequest[k].id}"><div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${requestedProduct.result[0].images[0].image}" alt="picture of ${requestedProduct.result[0].product_name}" /></div><div class="word-section"><p class="product-name">${requestedProduct.result[0].product_name}`;
          if (preview.fetchSendRequest[k].accept === true) {
            sendRequestHTML += `<br/><span class="accepted-request">ACEEPTED</span>`;
          }
          sendRequestHTML += `</p><i class="fas fa-sign-in-alt click-in-for-more"></i></div></div></a>`;
        }
      }
      sendRequestHTML += `</div>`;
      sendRequest.innerHTML = sendRequestHTML;
    }

    const notConfirmRequestFromTargetUser = checkCompleteRequest.targetUser.filter(
      (e) => {
        return e.confirm_from_target_user === false;
      }
    );
    const notConfirmRequestFromRequester = checkCompleteRequest.requester.filter(
      (e) => {
        return e.confirm_from_requester === false;
      }
    );
    let checkCompleteRequestHTML = "";
    if (notConfirmRequestFromTargetUser.length !== 0) {
      checkCompleteRequestHTML += `<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Completed Request</i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
    } else if (notConfirmRequestFromRequester.length !== 0) {
      checkCompleteRequestHTML += `<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Completed Request</i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
    }

    if (notConfirmRequestFromTargetUser.length !== 0) {
      for (let a = 0; a < notConfirmRequestFromTargetUser.length; a++) {
        checkCompleteRequestHTML += `<div class="exchange-request-div"><a href="/exchange/record/${notConfirmRequestFromTargetUser[a].id}"><div class="exchange-request-image-div"><img src="/${notConfirmRequestFromTargetUser[a].visitor_product_image}" alt="picture of ${notConfirmRequestFromTargetUser[a].visitor_product_name}"/></div></a><div><h5 class="product-name-confirm">${notConfirmRequestFromTargetUser[a].visitor_product_name}</h5><form action="/exchange/to-confirm-complete-request-from-target/${notConfirmRequestFromTargetUser[a].id}?_method=PUT"" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form></div></div>`;
      }
    }

    if (notConfirmRequestFromRequester.length !== 0) {
      for (let b = 0; b < notConfirmRequestFromRequester.length; b++) {
        checkCompleteRequestHTML += `<div class="exchange-request-div"><a href="/exchange/record/${notConfirmRequestFromRequester[b].id}"><div class="exchange-request-image-div"><img src="/${notConfirmRequestFromRequester[b].target_product_image}" alt="picture of ${notConfirmRequestFromRequester[b].target_product_name}"/></div></a><div><h5 class="product-name-confirm">${notConfirmRequestFromRequester[b].target_product_name}</h5><form action="/exchange/to-confirm-complete-request-from-request/${notConfirmRequestFromRequester[b].id}?_method=PUT"" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form></div></div>`;
      }
    }
    if (notConfirmRequestFromTargetUser.length !== 0) {
      checkCompleteRequestHTML += `</div>`;
      notificationForCompletedRequest.innerHTML = checkCompleteRequestHTML;
    } else if (notConfirmRequestFromRequester.length !== 0) {
      checkCompleteRequestHTML += `</div>`;
      notificationForCompletedRequest.innerHTML = checkCompleteRequestHTML;
    }
  } else {
    profilePreview.innerHTML = "";
    logDiv.innerHTML = `<a href="/register" class="a-tag">Register</a>
        <a href="/login" class="a-tag">Login</a>`;
  }
};
fetchData();

const passwordSection = document.querySelector(".password-section");
const checkIfGoogle = async () => {
  const data = await fetch("/profile/user-detail");
  const preview = await data.json();
  return preview;
};
const setHTML = async () => {
  const preview = await checkIfGoogle();
  let imageAddress;
  if (preview[0].profilepicture.slice(0, 5) === "https") {
    imageAddress = preview[0].profilepicture;
  } else {
    imageAddress = "/" + preview[0].profilepicture;
  }
  profilePreview.innerHTML = `<a href="/profile?id=${preview[0].id}"><div class="image">
                <img src="${imageAddress}"/></div>
                <p class="preview-name">${preview[0].username}</p>
                </a>`;
  if (preview[0].is_google) {
    passwordSection.setAttribute("hidden", "true");
  }
};

setHTML();
