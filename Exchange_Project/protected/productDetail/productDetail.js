const humanize = (str) => {
  let i,
    frags = str.split("_");
  for (i = 0; i < frags.length; i++) {
    frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
  }
  return frags.join(" ").replace("And", "&");
};
const categorySection = document.querySelector(".category-section");
const profilePreview = document.querySelector(".profile-preview");
const logDiv = document.querySelector(".log-div");
const requestSection = document.querySelector(".request-section");
const notificationForReject = document.querySelector(".notification-reject");
const notificationForWithdraw = document.querySelector(
  ".notification-withdraw"
);
const sendRequest = document.querySelector(".request-condition");
const notificationForCompletedRequest = document.querySelector(
  ".notification-completed-request"
);
const fetchData = async () => {
  const data = await fetch("/check-log");
  const preview = await data.json();
  const categoryDataJSON = await fetch("/category");
  const categoryData = await categoryDataJSON.json();
  let optionHTML = "";
  for (let c = 0; c < categoryData.length; c++) {
    optionHTML += `<a class="dropdown-item" href="/search?cate=${
      categoryData[c].id
    }">${humanize(categoryData[c].name)}</a>`;
  }
  categorySection.innerHTML = optionHTML;

  if (preview.result !== "none") {
    const checkRejectRequest = await fetch("/exchange/check-reject-request");
    const rejectedRequest = await checkRejectRequest.json();
    const checkWithdrawRequest = await fetch(
      "/exchange/check-withdraw-request"
    );
    const withdrewRequest = await checkWithdrawRequest.json();
    const checkCompletedRequestJSON = await fetch("/exchange/check-confirm");
    const checkCompleteRequest = await checkCompletedRequestJSON.json();
    let addressImage = "";
    if (preview.fetchedData[0].profilepicture.slice(0, 5) === "https") {
      addressImage = preview.fetchedData[0].profilepicture;
    } else {
      addressImage = "/" + preview.fetchedData[0].profilepicture;
    }
    profilePreview.innerHTML = `<a href="/profile?id=${preview.fetchedData[0].id}"><div class="image"><img src="${addressImage}" /></div><p class="preview-name">${preview.fetchedData[0].username}</p></a>`;
    logDiv.innerHTML = `<a href="/logout" class="log-out-tag">Logout</a>`;
    if (rejectedRequest.length !== 0) {
      let notificationRejectHTML = `<a class="nav-link dropdown-toggle notification" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Request Rejected <i class="fas exclamation fa-exclamation"></i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let h = 0; h < rejectedRequest.length; h++) {
        notificationRejectHTML += `<div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${rejectedRequest[h][0].product_image}" alt="picture of ${rejectedRequest[h][0].product_name}"/></div><div><p class="product-name-confirm">${rejectedRequest[h][0].product_name}</p>
                <form action="/exchange/confirm-reject-withdraw/${rejectedRequest[h][1]}?_method=DELETE" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form>
                </div></div>`;
      }
      notificationRejectHTML += `</div>`;
      notificationForReject.innerHTML = notificationRejectHTML;
    }
    if (withdrewRequest.length !== 0) {
      let notificationWithdrawHTML = `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Withdrawn Request <i class="fas exclamation fa-exclamation"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let j = 0; j < withdrewRequest.length; j++) {
        console.log();
        notificationWithdrawHTML += `<div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${withdrewRequest[j][0].product_image}" alt="picture of ${withdrewRequest[j][0].product_name}"/></div><div><p class="product-name-confirm">${withdrewRequest[j][0].product_name}</p>
              <form action="/exchange/confirm-reject-withdraw/${withdrewRequest[j][1]}?_method=DELETE" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form>
              </div></div>`;
      }
      notificationWithdrawHTML += `</div>`;
      notificationForWithdraw.innerHTML = notificationWithdrawHTML;
    }

    if (preview.fetchRequest.length !== 0) {
      let navbarRequestHTML = "";
      navbarRequestHTML += `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Exchange Request <i class="fas exclamation fa-exclamation"></i>
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let i = 0; i < preview.fetchRequest.length; i++) {
        if (
          !preview.fetchRequest[i].reject &&
          !preview.fetchRequest[i].withdraw
        ) {
          const requestedProductData = await fetch(
            "/detail/product/sketch/" +
              preview.fetchRequest[i].target_product_id
          );
          const requestedProduct = await requestedProductData.json();
          navbarRequestHTML += `<a class="dropdown-item" href="/exchange/receive?requester_product_id=${preview.fetchRequest[i].visitor_product_id}&target_product_id=${preview.fetchRequest[i].target_product_id}&request_data_id=${preview.fetchRequest[i].id}"><div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${requestedProduct.result[0].images[0].image}" alt="picture of ${requestedProduct.result[0].product_name}" /></div><div class="word-section"><p class="product-name">${requestedProduct.result[0].product_name}</p><i class="fas fa-sign-in-alt click-in-for-more"></i></div></div></a>`;
        }
      }

      navbarRequestHTML += `</div>`;
      requestSection.innerHTML = navbarRequestHTML;
    }
    if (preview.fetchSendRequest.length !== 0) {
      let sendRequestHTML = "";
      sendRequestHTML += `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Sent Request</i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let k = 0; k < preview.fetchSendRequest.length; k++) {
        if (
          !preview.fetchSendRequest[k].reject &&
          !preview.fetchSendRequest[k].withdraw
        ) {
          const requestedProductData = await fetch(
            "/detail/product/sketch/" +
              preview.fetchSendRequest[k].target_product_id
          );
          const requestedProduct = await requestedProductData.json();
          sendRequestHTML += `<a class="dropdown-item" href="/exchange/to-exchange?requester_product_id=${preview.fetchSendRequest[k].visitor_product_id}&target_product_id=${preview.fetchSendRequest[k].target_product_id}&request_data_id=${preview.fetchSendRequest[k].id}"><div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${requestedProduct.result[0].images[0].image}" alt="picture of ${requestedProduct.result[0].product_name}" /></div><div class="word-section"><p class="product-name">${requestedProduct.result[0].product_name}`;
          if (preview.fetchSendRequest[k].accept === true) {
            sendRequestHTML += `<br/><span class="accepted-request">ACEEPTED</span>`;
          }
          sendRequestHTML += `</p><i class="fas fa-sign-in-alt click-in-for-more"></i></div></div></a>`;
        }
      }
      sendRequestHTML += `</div>`;
      sendRequest.innerHTML = sendRequestHTML;
    }

    const notConfirmRequestFromTargetUser = checkCompleteRequest.targetUser.filter(
      (e) => {
        return e.confirm_from_target_user === false;
      }
    );
    const notConfirmRequestFromRequester = checkCompleteRequest.requester.filter(
      (e) => {
        return e.confirm_from_requester === false;
      }
    );
    let checkCompleteRequestHTML = "";
    if (notConfirmRequestFromTargetUser.length !== 0) {
      checkCompleteRequestHTML += `<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Completed Request</i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
    } else if (notConfirmRequestFromRequester.length !== 0) {
      checkCompleteRequestHTML += `<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Completed Request</i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
    }

    if (notConfirmRequestFromTargetUser.length !== 0) {
      for (let a = 0; a < notConfirmRequestFromTargetUser.length; a++) {
        checkCompleteRequestHTML += `<div class="exchange-request-div"><a href="/exchange/record/${notConfirmRequestFromTargetUser[a].id}"><div class="exchange-request-image-div"><img src="/${notConfirmRequestFromTargetUser[a].visitor_product_image}" alt="picture of ${notConfirmRequestFromTargetUser[a].visitor_product_name}"/></div></a><div><h5 class="product-name-confirm">${notConfirmRequestFromTargetUser[a].visitor_product_name}</h5><form action="/exchange/to-confirm-complete-request-from-target/${notConfirmRequestFromTargetUser[a].id}?_method=PUT"" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form></div></div>`;
      }
    }

    if (notConfirmRequestFromRequester.length !== 0) {
      for (let b = 0; b < notConfirmRequestFromRequester.length; b++) {
        checkCompleteRequestHTML += `<div class="exchange-request-div"><a href="/exchange/record/${notConfirmRequestFromRequester[b].id}"><div class="exchange-request-image-div"><img src="/${notConfirmRequestFromRequester[b].target_product_image}" alt="picture of ${notConfirmRequestFromRequester[b].target_product_name}"/></div></a><div><h5 class="product-name-confirm">${notConfirmRequestFromRequester[b].target_product_name}</h5><form action="/exchange/to-confirm-complete-request-from-request/${notConfirmRequestFromRequester[b].id}?_method=PUT"" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form></div></div>`;
      }
    }
    if (notConfirmRequestFromTargetUser.length !== 0) {
      checkCompleteRequestHTML += `</div>`;
      notificationForCompletedRequest.innerHTML = checkCompleteRequestHTML;
    } else if (notConfirmRequestFromRequester.length !== 0) {
      checkCompleteRequestHTML += `</div>`;
      notificationForCompletedRequest.innerHTML = checkCompleteRequestHTML;
    }
  } else {
    profilePreview.innerHTML = "";
    logDiv.innerHTML = `<a href="/register" class="a-tag">Register</a>
        <a href="/login" class="a-tag">Login</a>`;
  }
};
fetchData();

// template js
function errorMessage() {
  const searchParams = new URLSearchParams(window.location.search);
  const errMessage = searchParams.get("error");

  if (errMessage) {
    const alertBox = document.createElement("div");
    alertBox.classList.add("alert", "alert-danger");
    alertBox.textContent = errMessage;
    document.querySelector("#error-message").appendChild(alertBox);
  } else {
    document.querySelector("#error-message").innerHTML = "";
  }
}
errorMessage();
const imageSection = document.querySelector(".image-section");
const userInfo = document.querySelector(".user-info");
const productInfo = document.querySelector(".product-info");
const likeSection = document.querySelector(".for-like");
const likeBtn = document.querySelector(".likeBtn");
const likeCounting = document.querySelector(".like-counting");
const requestBtn = document.querySelector(".requestBtn");
const commentInput = document.querySelector(".comment-input");
const commentSection = document.querySelector(".comment-section");
const hiddenForm = document.querySelector(".hiddenForm");
const nextPage = document.querySelector(".next");
const previousPage = document.querySelector(".previous");
const toHiddenSection = document.querySelector(".tohidden");
const cross = document.querySelector(".cross");
let startIndex = 0;
let endIndex = 2;

const fetchProductDetail = async () => {
  const searchParams = new URLSearchParams(location.search);
  let id = searchParams.get("id");
  const data = await fetch("/detail/product/" + id);
  const productDetail = await data.json();

  const fetchLikeData = await fetch(
    "/check-like/" + productDetail.result[0].product_id
  );
  const likeData = await fetchLikeData.json();
  const fetchCommentData = await fetch(
    "/check-comment/" + productDetail.result[0].product_id
  );

  const commentData = await fetchCommentData.json();
  const productData = await fetch("/detail/data/" + productDetail.visitorId);
  const productPreview = await productData.json();

  let imageHTML = `<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>`;
  for (let i = 1; i < productDetail.result[0].images.length; i++) {
    imageHTML += `<li data-target="#carouselExampleIndicators" data-slide-to="${i}"></li>`;
  }
  imageHTML += `</ol>
        <div class="carousel-inner">
        <div class="carousel-item active">
      <img src="/${productDetail.result[0].images[0].image}" class="d-block w-100" alt="picture of (${productDetail.result[0].product_name})">
    </div>`;
  for (let k = 1; k < productDetail.result[0].images.length; k++) {
    imageHTML += `<div class="carousel-item each-image">
        <img src="/${productDetail.result[0].images[k].image}"  class="d-block w-100" alt="picture of (${productDetail.result[0].product_name})">
      </div>`;
  }
  imageHTML += `</div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>`;
  imageSection.innerHTML = imageHTML;

  rateAverage(productDetail.user, productDetail.result[0]);

  productInfo.innerHTML = `<div><h2 class="texts"><span class="main-name">${
    productDetail.result[0].product_name
  }</h2><p class="texts">Location: <span class="info">${
    productDetail.result[0].location
  }</span></p><p class="texts">Description: <span class="info">${
    productDetail.result[0].description
  }</span></p><p class="texts">Trade wishing list: <span class="info">${
    productDetail.result[0].wishedForExchange === null
      ? ""
      : productDetail.result[0].wishedForExchange
  }</span></p><p class="texts">Date of upload: <span class="info">${productDetail.result[0].created_at.slice(
    0,
    10
  )}</span></p></div>`;

  if (
    productDetail.visitorId === undefined ||
    parseInt(productDetail.result[0].product_user_id) ===
      parseInt(productDetail.visitorId)
  ) {
    likeBtn.classList.add("likeHidden");
    requestBtn.classList.add("likeHidden");
  }

  commentInput.innerHTML = `<div class="make-comment"><form action="/comment/${parseInt(
    productDetail.result[0].product_id
  )}" method="POST" class="form-comment">
        <div>
        <label for="comment"></label>
        <textarea wrap="off" name="comment" placeholder="comment" ></textarea>
        </div>
        <div class="input-comment-div">
        <input class="submit-comment" type="submit" value="Submit"/>
        </div>
      </form></div>`;

  let commentHTML = "";
  for (let j = 0; j < commentData.result.length; j++) {
    let imageAddress = "";
    if (commentData.userArr[j][0].profilepicture.slice(0, 5) === "https") {
      imageAddress = commentData.userArr[j][0].profilepicture;
    } else {
      imageAddress = "/" + commentData.userArr[j][0].profilepicture;
    }
    commentHTML += `<div class="comment-data">
                      <div class="profile-comment">
                          <h4>
                            <a href="/profile?id=${commentData.userArr[j][0].id}">${commentData.userArr[j][0].username}
                            </a>
                          </h4>
                        <div class="profile-image">
                      <img src="${imageAddress}" alt="profile's picture of ${commentData.userArr[j][0].username}"/>
                    </div>
                  </div>
                <div class="comment-content"><p>${commentData.result[j].content}</p></div>`;
    if (
      parseInt(commentData.userArr[j][0].id) ===
      parseInt(productDetail.visitorId)
    ) {
      commentHTML += `<form action="/comment/delete/${parseInt(
        productDetail.result[0].product_id
      )}/${
        commentData.result[j].id
      }?_method=DELETE" method="POST"><input type="submit" class="delete-btn btn btn-danger" value="Delete"/></form>`;
    }

    commentHTML += `</div>`;
  }
  commentSection.innerHTML = commentHTML;
  const checkIfLike = likeData.result.some((e) => {
    return parseInt(e.user_id) === parseInt(productDetail.visitorId);
  });

  const likeFuntion = (checkIfLike, likeNum) => {
    if (checkIfLike) {
      likeBtn.classList.remove("far");
      likeBtn.classList.add("fas");
    } else {
      likeBtn.classList.add("far");
      likeBtn.classList.remove("fas");
    }
    likeCounting.innerHTML = `${likeNum} ${
      likeNum > 1 ? "users like" : "user likes"
    } this`;
  };

  likeBtn.addEventListener("click", async () => {
    const fetchLikeData = await fetch(
      "/check-like/" + productDetail.result[0].product_id
    );
    const likeData = await fetchLikeData.json();

    const checkIfLike = likeData.result.some((e) => {
      return parseInt(e.user_id) === parseInt(productDetail.visitorId);
    });
    if (checkIfLike) {
      await fetch("/dislike/" + parseInt(productDetail.result[0].product_id), {
        method: "POST",
      });
      likeFuntion(!checkIfLike, likeData.result.length - 1);
    } else {
      await fetch("/like/" + parseInt(productDetail.result[0].product_id), {
        method: "POST",
      });
      likeFuntion(!checkIfLike, likeData.result.length + 1);
    }
  });
  likeFuntion(checkIfLike, likeData.result.length);

  // Request section
  const turnPage = (productArr) => {
    let productPreviewHTML = "";
    if (productArr.length !== 0) {
      for (let i = 0; i < productArr.length; i++) {
        productPreviewHTML += `<div class="product-grid">
               <div class="image-section-div">
                <img src="/${productArr[i].images[0].image}" alt="picture of ${
          productArr[i].product_name
        }">
               </div>
               <div class="product-info">
               <h3 class="name-of-product">${productArr[i].product_name}</h3>
               <p>Date of upload: <span class="note-text">${productArr[
                 i
               ].created_at.slice(0, 10)}</span></p>
              </div>
              <form action="/exchange/request/${productArr[i].product_id}/${
          productDetail.result[0].product_id
        }/${
          productDetail.result[0].product_user_id
        }" method="POST" class="delete-form">
              <div>
              <input type="submit" class=" btn btn-warning" value="Send Request"/>
              </div>
              </form>
              </div>`;
      }
    } else {
      productPreviewHTML = `<h5>You have no goods for trade.</h5>`;
    }
    hiddenForm.innerHTML = productPreviewHTML;
  };
  cross.addEventListener("click", () => {
    // toHiddenSection.style.display = "none";
    toHiddenSection.classList.remove("show-div");
  });

  toHiddenSection.addEventListener("click", (e) => {
    if (e.target.classList[0] === "to-center") {
      toHiddenSection.classList.remove("show-div");
    }

    // toHiddenSection.style.display = "none";
  });
  console.log("productDetail", productDetail)
  if(!productDetail.result[0].acceptedForExchange){
  requestBtn.addEventListener("click", () => {
    // toHiddenSection.style.display = "initial";
    
      toHiddenSection.classList.add("show-div");
  });
}else{
  requestBtn.disabled = true
  requestBtn.classList.add("can_not_request")
}
  const randerButton = () => {
    if (productPreview.result.length <= 2) {
      previousPage.classList.add("hidden");
      nextPage.classList.add("hidden");
    } else if (2 > startIndex) {
      previousPage.classList.add("hidden");
      nextPage.classList.remove("hidden");
    } else if (productPreview.result.length <= endIndex) {
      previousPage.classList.remove("hidden");
      nextPage.classList.add("hidden");
    } else {
      previousPage.classList.remove("hidden");
      nextPage.classList.remove("hidden");
    }
  };
  const renderPage = () => {
    const showThreePages = productPreview.result.slice(startIndex, endIndex);
    turnPage(showThreePages);
  };
  randerButton();
  renderPage();
  previousPage.addEventListener("click", () => {
    startIndex -= 2;
    endIndex -= 2;
    randerButton();
    renderPage();
  });
  nextPage.addEventListener("click", () => {
    startIndex += 2;
    endIndex += 2;
    randerButton();
    renderPage();
  });
};

fetchProductDetail();

function rateAverage(num, user) {
  let starStr = "";
  let targetNumSum = 0;
  let numUser = "";
  const fullStar = `<i class="fas star-icon fa-star"></i>`;
  const emptyStar = `<i class="far star-icon fa-star"></i>`;
  if (num.length !== 0) {
    for (let i = 0; i < num.length; i++) {
      targetNumSum += num[i].rating;
    }
    const averageNum = targetNumSum / num.length;

    for (let k = 0; k < Math.round(averageNum); k++) {
      starStr += fullStar;
    }
    for (let j = 0; j < 5 - Math.round(averageNum); j++) {
      starStr += emptyStar;
    }
    num.length < 2 ? (numUser = "user") : (numUser = "users");
  } else {
    for (let j = 0; j < 5; j++) {
      starStr += emptyStar;
    }
    numUser = "user";
  }

  userInfo.innerHTML = `<div><h3><a href="/profile?id=${user.product_user_id}" class="owner-name">${user.usernames[0].user}</a></h3></div><div class="star-holder">${starStr}</div>`;
}
