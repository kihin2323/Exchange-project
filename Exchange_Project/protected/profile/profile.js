window.onload = function () {
  const searchParams = new URLSearchParams(window.location.search);
  const errMessage = searchParams.get("error");

  if (errMessage) {
    const alertBox = document.createElement("div");
    alertBox.classList.add("alert", "alert-danger");
    alertBox.textContent = errMessage;
    document.querySelector("#error-message").appendChild(alertBox);
  } else {
    document.querySelector("#error-message").innerHTML = "";
  }
};

const humanize = (str) => {
  let i,
    frags = str.split("_");
  for (i = 0; i < frags.length; i++) {
    frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
  }
  return frags.join(" ").replace("And", "&");
};
const categorySection = document.querySelector(".category-section");
const profilePreview = document.querySelector(".profile-preview");
const logDiv = document.querySelector(".log-div");
const requestSection = document.querySelector(".request-section");
const notificationForReject = document.querySelector(".notification-reject");
const notificationForWithdraw = document.querySelector(
  ".notification-withdraw"
);
const sendRequest = document.querySelector(".request-condition");
const notificationForCompletedRequest = document.querySelector(
  ".notification-completed-request"
);
const fetchData = async () => {
  const data = await fetch("/check-log");
  const preview = await data.json();
  const categoryDataJSON = await fetch("/category");
  const categoryData = await categoryDataJSON.json();
  let optionHTML = "";
  for (let c = 0; c < categoryData.length; c++) {
    optionHTML += `<a class="dropdown-item" href="/search?cate=${
      categoryData[c].id
    }">${humanize(categoryData[c].name)}</a>`;
  }
  categorySection.innerHTML = optionHTML;

  if (preview.result !== "none") {
    const checkRejectRequest = await fetch("/exchange/check-reject-request");
    const rejectedRequest = await checkRejectRequest.json();
    const checkWithdrawRequest = await fetch(
      "/exchange/check-withdraw-request"
    );
    const withdrewRequest = await checkWithdrawRequest.json();
    const checkCompletedRequestJSON = await fetch("/exchange/check-confirm");
    const checkCompleteRequest = await checkCompletedRequestJSON.json();
    let addressImage = "";
    if (preview.fetchedData[0].profilepicture.slice(0, 5) === "https") {
      addressImage = preview.fetchedData[0].profilepicture;
    } else {
      addressImage = "/" + preview.fetchedData[0].profilepicture;
    }
    profilePreview.innerHTML = `<a href="/profile?id=${preview.fetchedData[0].id}"><div class="image"><img src="${addressImage}" /></div><p class="preview-name">${preview.fetchedData[0].username}</p></a>`;
    logDiv.innerHTML = `<a href="/logout" class="log-out-tag">Logout</a>`;
    if (rejectedRequest.length !== 0) {
      let notificationRejectHTML = `<a class="nav-link dropdown-toggle notification" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Request Rejected <i class="fas exclamation fa-exclamation"></i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let h = 0; h < rejectedRequest.length; h++) {
        notificationRejectHTML += `<div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${rejectedRequest[h][0].product_image}" alt="picture of ${rejectedRequest[h][0].product_name}"/></div><div><p class="product-name-confirm">${rejectedRequest[h][0].product_name}</p>
                <form action="/exchange/confirm-reject-withdraw/${rejectedRequest[h][1]}?_method=DELETE" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form>
                </div></div>`;
      }
      notificationRejectHTML += `</div>`;
      notificationForReject.innerHTML = notificationRejectHTML;
    }
    if (withdrewRequest.length !== 0) {
      let notificationWithdrawHTML = `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Withdrawn Request <i class="fas exclamation fa-exclamation"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let j = 0; j < withdrewRequest.length; j++) {
        console.log();
        notificationWithdrawHTML += `<div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${withdrewRequest[j][0].product_image}" alt="picture of ${withdrewRequest[j][0].product_name}"/></div><div><p class="product-name-confirm">${withdrewRequest[j][0].product_name}</p>
              <form action="/exchange/confirm-reject-withdraw/${withdrewRequest[j][1]}?_method=DELETE" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form>
              </div></div>`;
      }
      notificationWithdrawHTML += `</div>`;
      notificationForWithdraw.innerHTML = notificationWithdrawHTML;
    }

    if (preview.fetchRequest.length !== 0) {
      let navbarRequestHTML = "";
      navbarRequestHTML += `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Exchange Request <i class="fas exclamation fa-exclamation"></i>
      </a>
      <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let i = 0; i < preview.fetchRequest.length; i++) {
        if (
          !preview.fetchRequest[i].reject &&
          !preview.fetchRequest[i].withdraw
        ) {
          const requestedProductData = await fetch(
            "/detail/product/sketch/" +
              preview.fetchRequest[i].target_product_id
          );
          const requestedProduct = await requestedProductData.json();
          navbarRequestHTML += `<a class="dropdown-item" href="/exchange/receive?requester_product_id=${preview.fetchRequest[i].visitor_product_id}&target_product_id=${preview.fetchRequest[i].target_product_id}&request_data_id=${preview.fetchRequest[i].id}"><div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${requestedProduct.result[0].images[0].image}" alt="picture of ${requestedProduct.result[0].product_name}" /></div><div class="word-section"><p class="product-name">${requestedProduct.result[0].product_name}</p><i class="fas fa-sign-in-alt click-in-for-more"></i></div></div></a>`;
        }
      }

      navbarRequestHTML += `</div>`;
      requestSection.innerHTML = navbarRequestHTML;
    }
    if (preview.fetchSendRequest.length !== 0) {
      let sendRequestHTML = "";
      sendRequestHTML += `<a class="nav-link notification dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Sent Request</i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
      for (let k = 0; k < preview.fetchSendRequest.length; k++) {
        if (
          !preview.fetchSendRequest[k].reject &&
          !preview.fetchSendRequest[k].withdraw
        ) {
          const requestedProductData = await fetch(
            "/detail/product/sketch/" +
              preview.fetchSendRequest[k].target_product_id
          );
          const requestedProduct = await requestedProductData.json();
          sendRequestHTML += `<a class="dropdown-item" href="/exchange/to-exchange?requester_product_id=${preview.fetchSendRequest[k].visitor_product_id}&target_product_id=${preview.fetchSendRequest[k].target_product_id}&request_data_id=${preview.fetchSendRequest[k].id}"><div class="exchange-request-div"><div class="exchange-request-image-div"><img src="/${requestedProduct.result[0].images[0].image}" alt="picture of ${requestedProduct.result[0].product_name}" /></div><div class="word-section"><p class="product-name">${requestedProduct.result[0].product_name}`;
          if (preview.fetchSendRequest[k].accept === true) {
            sendRequestHTML += `<br/><span class="accepted-request">ACEEPTED</span>`;
          }
          sendRequestHTML += `</p><i class="fas fa-sign-in-alt click-in-for-more"></i></div></div></a>`;
        }
      }
      sendRequestHTML += `</div>`;
      sendRequest.innerHTML = sendRequestHTML;
    }

    const notConfirmRequestFromTargetUser = checkCompleteRequest.targetUser.filter(
      (e) => {
        return e.confirm_from_target_user === false;
      }
    );
    const notConfirmRequestFromRequester = checkCompleteRequest.requester.filter(
      (e) => {
        return e.confirm_from_requester === false;
      }
    );
    let checkCompleteRequestHTML = "";
    if (notConfirmRequestFromTargetUser.length !== 0) {
      checkCompleteRequestHTML += `<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Completed Request</i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
    } else if (notConfirmRequestFromRequester.length !== 0) {
      checkCompleteRequestHTML += `<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Completed Request</i>
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">`;
    }

    if (notConfirmRequestFromTargetUser.length !== 0) {
      for (let a = 0; a < notConfirmRequestFromTargetUser.length; a++) {
        checkCompleteRequestHTML += `<div class="exchange-request-div"><a href="/exchange/record/${notConfirmRequestFromTargetUser[a].id}"><div class="exchange-request-image-div"><img src="/${notConfirmRequestFromTargetUser[a].visitor_product_image}" alt="picture of ${notConfirmRequestFromTargetUser[a].visitor_product_name}"/></div></a><div><h5 class="product-name-confirm">${notConfirmRequestFromTargetUser[a].visitor_product_name}</h5><form action="/exchange/to-confirm-complete-request-from-target/${notConfirmRequestFromTargetUser[a].id}?_method=PUT"" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form></div></div>`;
      }
    }

    if (notConfirmRequestFromRequester.length !== 0) {
      for (let b = 0; b < notConfirmRequestFromRequester.length; b++) {
        checkCompleteRequestHTML += `<div class="exchange-request-div"><a href="/exchange/record/${notConfirmRequestFromRequester[b].id}"><div class="exchange-request-image-div"><img src="/${notConfirmRequestFromRequester[b].target_product_image}" alt="picture of ${notConfirmRequestFromRequester[b].target_product_name}"/></div></a><div><h5 class="product-name-confirm">${notConfirmRequestFromRequester[b].target_product_name}</h5><form action="/exchange/to-confirm-complete-request-from-request/${notConfirmRequestFromRequester[b].id}?_method=PUT"" method="POST"><input type="submit" class="btn btn-dark confirm-btn" value="Confirm"/></form></div></div>`;
      }
    }
    if (notConfirmRequestFromTargetUser.length !== 0) {
      checkCompleteRequestHTML += `</div>`;
      notificationForCompletedRequest.innerHTML = checkCompleteRequestHTML;
    } else if (notConfirmRequestFromRequester.length !== 0) {
      checkCompleteRequestHTML += `</div>`;
      notificationForCompletedRequest.innerHTML = checkCompleteRequestHTML;
    }
  } else {
    profilePreview.innerHTML = "";
    logDiv.innerHTML = `<a href="/register" class="a-tag">Register</a>
        <a href="/login" class="a-tag">Login</a>`;
  }
};
fetchData();

const userProfile = document.querySelector(".profile-info");
const Rating = document.querySelector(".rating");
const giveRating = document.querySelector(".give-rating");
const editProfileDiv = document.querySelector(".edit-profile-div");
const uploadOption = document.querySelector(".upload-option");
const uploadProductSection = document.querySelector(".uploaded-product");
const nextPage = document.querySelector(".next");
const previousPage = document.querySelector(".previous");
const transactionRecord = document.querySelector(".history");
const hostoryTag = document.querySelector(".history-tag");
let startIndex = 0;
let endIndex = 2;
const checkIfGivenRate = (targetId, visitorId) => {
  giveRating.innerHTML = `<form action="/profile/give-rating/${targetId}/${visitorId}" method="POST">
                            <label for="rating">Rate from 1-5:</label>
                            <select name="rating" class="rate-selector">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <input class="btn btn-outline-success rate-btn" type="submit" value="Give Rate"/>
                            </form>`;
};
const rateAverage = (num) => {
  let starStr = "";
  let targetNumSum = 0;
  let numUser = "";
  const fullStar = `<i class="fas star-icon fa-star"></i>`;
  const emptyStar = `<i class="far star-icon fa-star"></i>`;
  if (num.length !== 0) {
    for (let i = 0; i < num.length; i++) {
      targetNumSum += num[i].rating;
    }
    const averageNum = targetNumSum / num.length;

    for (let k = 0; k < Math.round(averageNum); k++) {
      starStr += fullStar;
    }
    for (let j = 0; j < 5 - Math.round(averageNum); j++) {
      starStr += emptyStar;
    }
  } else {
    for (let j = 0; j < 5; j++) {
      starStr += emptyStar;
    }
  }

  num.length < 2 ? (numUser = "user") : (numUser = "users");
  Rating.innerHTML = `<div><p>${num.length} ${numUser} rated</p></div><div class="star-holder">${starStr}</div>`;
};

const showGivenRate = (ratingNum) => {
  giveRating.innerHTML = `<p>You rated this user: ${ratingNum}</p>`;
};

async function fetchTargetUserData() {
  const searchParams = new URLSearchParams(location.search);
  let id = searchParams.get("id");
  const data = await fetch("/profile/user-detail/" + id);
  const profileDetail = await data.json();
  const previewData = await fetch("/profile/user-detail");
  const preview = await previewData.json();
  const productData = await fetch("/detail/data/" + id);
  const productPreview = await productData.json();
  const transactionHistoryJSON = await fetch("/exchange/check-confirm");
  const transactionHistory = await transactionHistoryJSON.json();
  console.log("profileDetail", profileDetail)
  console.log("preview", preview)
  console.log("productPreview", productPreview)
  console.log("transactionHistory", transactionHistory)
  let transactionPreviewHTML = ``;
  if (
    transactionHistory.targetUser.length !== 0 &&
    profileDetail.resultData[0].id === preview[0].id
  ) {
    hostoryTag.innerHTML = `<h6 class="historyTag">Transaction Record:</h6>`;
    for (let k = 0; k < transactionHistory.targetUser.length; k++) {
      transactionPreviewHTML += `<div class="history-section"><a href="/record?record_id=${transactionHistory.targetUser[k].id}&id=${preview[0].id}" class="history-record"><h5 class="history-h5">${transactionHistory.targetUser[k].target_product_name}</h5><i class="fas fa-exchange-alt arrow-icon"></i>
      <h5 class="history-h5">${transactionHistory.targetUser[k].visitor_product_name}</h5><p class="history-date">Date: ${transactionHistory.targetUser[k].date}</p></a></div>`;
    }
  }

  if (
    transactionHistory.requester.length !== 0 &&
    profileDetail.resultData[0].id === preview[0].id
  ) {
    hostoryTag.innerHTML = `<h6 class="historyTag">Transaction Record:</h6>`;
    for (let j = 0; j < transactionHistory.requester.length; j++) {
      transactionPreviewHTML += `<div class="history-section"><a href="/record?record_id=${transactionHistory.requester[j].id}&id=${preview[0].id}" class="history-record"><h5 class="history-h5">${transactionHistory.requester[j].visitor_product_name}</h5><i class="fas fa-exchange-alt arrow-icon"></i>
      <h5 class="history-h5">${transactionHistory.requester[j].target_product_name}</h5><p class="history-date">Date: ${transactionHistory.requester[j].date}</p></a></div>`;
    }
  }
  transactionRecord.innerHTML = transactionPreviewHTML;

  function isoDate(isoDate) {
    date = new Date(isoDate);
    let result =
      date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    return result;
  }
  let imageAddress = "";
  if (profileDetail.resultData[0].profilepicture.slice(0, 5) === "https") {
    imageAddress = profileDetail.resultData[0].profilepicture;
  } else {
    imageAddress = "/" + profileDetail.resultData[0].profilepicture;
  }
  userProfile.innerHTML = `<div class="profile-infomation"><div class="profile-image"><img class="image" src="${imageAddress}"><div>
                      <p class="user-name">${
                        profileDetail.resultData[0].username
                      }</p>
                      <p>${`Registered Date: ${isoDate(
                        profileDetail.resultData[0].created_at
                      )}`}</p>
                      </div>`;

  if (profileDetail.getRating) {
    const checkIfRated = profileDetail.getRating.find((e) => {
      return parseInt(e.visitor_id) === parseInt(preview[0].id);
    });
    if (checkIfRated && parseInt(id) !== parseInt(preview[0].id)) {
      showGivenRate(checkIfRated.rating);
    } else if (parseInt(id) !== parseInt(preview[0].id)) {
      checkIfGivenRate(id, preview[0].id);
    }
  }
  if (parseInt(id) === parseInt(preview[0].id)) {
    editProfileDiv.innerHTML = `<a href="/profile/update?id=${preview[0].id}" class="edit-button">Edit profile</a>`;
    uploadOption.innerHTML = `<a href="/profile/product/upload" class="upload-button" >Upload</a>`;
  }

  const randerButton = () => {
    if (productPreview.result.length <= 2) {
      previousPage.classList.add("hidden");
      nextPage.classList.add("hidden");
    } else if (2 > startIndex) {
      previousPage.classList.add("hidden");
      nextPage.classList.remove("hidden");
    } else if (productPreview.result.length <= endIndex) {
      previousPage.classList.remove("hidden");
      nextPage.classList.add("hidden");
    } else {
      previousPage.classList.remove("hidden");
      nextPage.classList.remove("hidden");
    }
  };

  const renderPage = () => {
    const showThreePages = productPreview.result.slice(startIndex, endIndex);
    turnPage(showThreePages, parseInt(id), parseInt(preview[0].id));
  };
  randerButton();
  renderPage();
  rateAverage(profileDetail.getRating);
  previousPage.addEventListener("click", () => {
    startIndex -= 2;
    endIndex -= 2;
    randerButton();
    renderPage();
  });
  nextPage.addEventListener("click", () => {
    startIndex += 2;
    endIndex += 2;
    randerButton();
    renderPage();
  });
}

const turnPage = (productArr, visitorId, AdminId) => {
  let productPreviewHTML = "";

  for (let i = 0; i < productArr.length; i++) {
    productPreviewHTML += `<div class="product-grid">
            <a href="/profile/detail?id=${
              productArr[i].product_id
            }" class="product-grid">
         <div class="image-section">
          <img src="${productArr[i].images[0].image}" alt="cars">
         </div>
         <div class="product-info">
         <h3 class="name-of-product">${productArr[i].product_name}</h3>
         <p><span class="note-text">Date of upload:</span> ${productArr[
           i
         ].created_at.slice(0, 10)}</p>
         <p><span class="note-text">Trade wishing list:</span> ${
           productArr[i].wishedForExchange === null
             ? ""
             : productArr[i].wishedForExchange
         }</p>
        </div>
        </a>`;
    if (visitorId === AdminId) {
      productPreviewHTML += `<form action="/profile/product/delete/${productArr[i].product_id}?_method=DELETE" method="POST" class="delete-form">
          <div><input class=" btn btn-danger" type="submit" value="Delete" /></div>
          </form>`;
    }
    productPreviewHTML += `</div></div>`;
  }
  uploadProductSection.innerHTML = productPreviewHTML;
};

fetchTargetUserData();
