import express from "express";
import path from "path";
import { hashPassword } from "./hash";
import { upload, knex } from "./server";

export const registerRoute = express.Router();

export const getRegister = async (
  req: express.Request,
  res: express.Response
) => {
  res.sendFile(path.resolve(__dirname, "./public/register.html"));
};

export const validUser = (email, password) => {
  const validEmail = typeof email === "string" && email.trim() !== "";
  const validPassword =
    typeof password === "string" &&
    password.trim() !== "" &&
    password.trim().length >= 6;
  return validEmail && validPassword;
};
export const toRegister = async (
  req: express.Request,
  res: express.Response
) => {
  const { email, name, password, confirmPassword } = req.body;
  if (password !== confirmPassword) {
    return res.redirect("/register.html?error=Passwords+must+match");
  }
  const checkEmail = await knex.select("*").from("users").where("email",email)
  if (checkEmail.length === 1) {
    return res.redirect("/register.html?error=email+is+registerd");
  }
  if (validUser(email, password)) {
    const hashedPassword = await hashPassword(password);
    await knex.insert({username: name,password: hashedPassword,profilepicture: req.file?.filename || "anonymous.png",email}).into("users")
  } else {
    res.redirect(
      "/register.html?error=Please+enter+a+valid+email+and+password"
    );
  }
  res.redirect("/login");
};

registerRoute.get("/", getRegister);
registerRoute.post("/", upload.single("image"), toRegister);
