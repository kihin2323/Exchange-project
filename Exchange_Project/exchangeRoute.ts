import express from "express";
import path from "path";
import {knex} from './server'

export const exchangeRoute = express.Router();

export const toExchange = (req: express.Request, res: express.Response) => {
  res.sendFile(
    path.resolve(
      __dirname,
      "protected/exchange-process/toExchange/toExchange.html"
    )
  );
};

export const choseWhatToExchange = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    const checkExchangeProduct = await knex.select("*").from("exchange_product_proposal").where("visitor_product_id",req.params.visit_product_id).andWhereNot("reject", true).andWhereNot("withdraw", true)
    const checkExchangeProductForAccepted = await knex.select("*").from("exchange_product_proposal").where("target_product_id",req.params.visit_product_id).andWhere("accept", true)

    const checkExchangeProductForAccepted2 = await knex.select("*").from("exchange_product_proposal").where("visitor_product_id",req.params.visit_product_id).andWhere("accept", true)
    if (
      checkExchangeProduct.length === 0 &&
      req.session &&
      checkExchangeProductForAccepted.length === 0 &&
      checkExchangeProductForAccepted2.length === 0
    ) {
      await knex.insert({target_product_id: req.params.target_product_id, target_product_user_id: req.params.target_product_user_id, user_id: req.session.user.id, visitor_product_id: req.params.visit_product_id, reject: false, accept: false, withdraw: false}).into("exchange_product_proposal")
      res.redirect("/profile/detail?id=" + req.params.target_product_id);
    } else {
      res
        .status(401)
        .redirect(
          "/profile/detail?id=" +
            req.params.target_product_id +
            "&error=This+Product+is+set+for+exchange+already."
        );
    }
  }catch(e){
    console.log("Referenzvorlagen: ", e)
  }
  
};

export const receiveRequest = (req: express.Request, res: express.Response) => {
  res.sendFile(
    path.resolve(
      __dirname,
      "protected/exchange-process/receiveRequest/receiveRequest.html"
    )
  );
};

export const requestAccepted = async (
  req: express.Request,
  res: express.Response
) => {
  try {
    const TargetSideRequest1 = await knex("exchange_product_proposal").update({reject: true}).where("target_product_id", req.params.target_product_id).andWhereNot("id", req.params.request_id).returning("id")
    const TargetSideRequest2 = await knex("exchange_product_proposal").update({withdraw: true}).where("target_product_id", req.params.target_product_id).andWhereNot("id", req.params.request_id).returning("id")
    const TargetSideRequest3 = await knex("exchange_product_proposal").update({reject: true}).where("visitor_product_id", req.params.target_product_id).andWhereNot("id", req.params.request_id).returning("id")
    const TargetSideRequest4 = await knex("exchange_product_proposal").update({withdraw: true}).where("visitor_product_id", req.params.target_product_id).andWhereNot("id", req.params.request_id).returning("id")

    const visitorSideRequest1 = await knex("exchange_product_proposal").update({reject: true}).where("visitor_product_id", req.params.visitor_product_id).andWhereNot("id", req.params.request_id).returning("id")
    const visitorSideRequest2 = await knex("exchange_product_proposal").update({withdraw: true}).where("visitor_product_id", req.params.visitor_product_id).andWhereNot("id", req.params.request_id).returning("id")
    const visitorSideRequest3 = await knex("exchange_product_proposal").update({reject: true}).where("target_product_id", req.params.visitor_product_id).andWhereNot("id", req.params.request_id).returning("id")
    const visitorSideRequest4 = await knex("exchange_product_proposal").update({withdraw: true}).where("target_product_id", req.params.visitor_product_id).andWhereNot("id", req.params.request_id).returning("id")

    timeSlotDeleteFunc(TargetSideRequest1);
    timeSlotDeleteFunc(TargetSideRequest2);
    timeSlotDeleteFunc(TargetSideRequest3);
    timeSlotDeleteFunc(TargetSideRequest4);
    timeSlotDeleteFunc(visitorSideRequest1);
    timeSlotDeleteFunc(visitorSideRequest2);
    timeSlotDeleteFunc(visitorSideRequest3);
    timeSlotDeleteFunc(visitorSideRequest4);
    await knex("product").update({accepted_for_exchange: true}).where("id", req.params.target_product_id)
    await knex("product").update({accepted_for_exchange: true}).where("id", req.params.visitor_product_id)
    await knex("exchange_product_proposal").update({accept: true}).where("id", req.params.request_id)
    res.redirect(
      "/exchange/receive?requester_product_id=" +
        req.params.visitor_product_id +
        "&target_product_id=" +
        req.params.target_product_id +
        "&request_data_id=" +
        req.params.request_id
    );
  } catch (e) {
    console.log("requestAccepted: ", e);
  }
};

async function timeSlotDeleteFunc(idArr: any[]) {
  for (let i = 0; i < idArr.length; i++) {
    await knex("suggested_time").where("exchange_product_proposal_id",idArr[i]).del()
  }
}

export const showConfirmTransaction = (
  req: express.Request,
  res: express.Response
) => {
  res.sendFile(
    path.resolve(
      __dirname,
      "protected/exchange-process/confirmTransaction/confirmTransaction.html"
    )
  );
};

export const storeTime = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    if(req.body.from_target_user){
      await knex.insert({exchange_product_proposal_id: req.params.request_id, content:req.body.content, date:req.body.date, time_from: req.body.time_from,time_to:req.body.time_to, location: req.body.location, from_target_user: req.body.from_target_user || req.body.from_requester  }).into("suggested_time")
    } else if(req.body.from_requester){
      await knex.insert({exchange_product_proposal_id: req.params.request_id, content:req.body.content, date:req.body.date, time_from: req.body.time_from,time_to:req.body.time_to, location: req.body.location, from_requester:  req.body.from_target_user || req.body.from_requester, accept: false }).into("suggested_time")
    }
    if (req.body.from_target_user) {
      res.redirect(
        `/exchange/receive?requester_product_id=${req.params.visitor_product_id}&target_product_id=${req.params.target_product_id}&request_data_id=${req.params.request_id}`
      );
    } else {
      res.redirect(
        `/exchange/to-exchange?requester_product_id=${req.params.visitor_product_id}&target_product_id=${req.params.target_product_id}&request_data_id=${req.params.request_id}`
      );
    }
  }catch(e){
    console.log("storeTime: ", e)
  }
};

export const cancelTimeSlot = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    await knex("suggested_time").update({accept: false}).where("id", req.params.time_slot_id)
    if (
      parseInt(req.session?.user.id) ===
      parseInt(req.params.target_product_user_id)
    ) {
      res.redirect(
        `/exchange/receive?requester_product_id=${req.params.visitor_product_id}&target_product_id=${req.params.target_product_id}&request_data_id=${req.params.request_id}`
      );
    } else {
      res.redirect(
        `/exchange/to-exchange?requester_product_id=${req.params.visitor_product_id}&target_product_id=${req.params.target_product_id}&request_data_id=${req.params.request_id}`
      );
    }
  }catch(e){
    console.log("cancelTimeSlot: ", e)
  }
};
export const acceptTimeSlot = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    await knex("suggested_time").update({accept: true}).where("id", req.params.time_slot_id)
    if (
      parseInt(req.session?.user.id) ===
      parseInt(req.params.target_product_user_id)
    ) {
      res.redirect(
        `/exchange/receive?requester_product_id=${req.params.visitor_product_id}&target_product_id=${req.params.target_product_id}&request_data_id=${req.params.request_id}`
      );
    } else {
      res.redirect(
        `/exchange/to-exchange?requester_product_id=${req.params.visitor_product_id}&target_product_id=${req.params.target_product_id}&request_data_id=${req.params.request_id}`
      );
    }
  }catch(e){
    console.log("acceptTimeSlot: ", e)
  }
};

export const getTimeSlot = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    const timeSlot = await knex.select("*").from("suggested_time").where("exchange_product_proposal_id", req.params.request_id);
    res.json(timeSlot);
  }catch(e){
    console.log("getTimeSlot: ", e)
  }
  
};

export const deleteTimeSlot = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    await knex("suggested_time").where("id", req.params.time_slot_id).del()
    if (
      parseInt(req.session?.user.id) ===
      parseInt(req.params.user_id)
    ) {
      res.redirect(
        `/exchange/receive?requester_product_id=${req.params.visitor_product_id}&target_product_id=${req.params.target_product_id}&request_data_id=${req.params.request_id}`
      );
    } else{
      res.redirect(
        `/exchange/to-exchange?requester_product_id=${req.params.visitor_product_id}&target_product_id=${req.params.target_product_id}&request_data_id=${req.params.request_id}`
      );
    }
  }catch(e){
    console.log("deleteTimeSlot: ", e)
  }
  
};

export const rejectRequest = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    await knex("suggested_time").where("exchange_product_proposal_id", req.params.request_id).del()
    await knex("exchange_product_proposal").update({reject: true}).where("id", req.params.request_id)
    res.redirect("/");
  }catch(e){
    console.log("rejectRequest: ", e)
  }
  
};

export const withdrawRequest = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    await knex("suggested_time").where("exchange_product_proposal_id", req.params.request_id).del()
    await knex("exchange_product_proposal").update({withdraw: true}).where("id", req.params.request_id)
    res.redirect("/");
  }catch(e){
    console.log("withdrawRequest: ", e)
  }
};
export const checkAcceptRequest = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    const data = await knex.select("accept").from("exchange_product_proposal").where("id", req.params.request_id)
    res.json(data[0]);
  }catch(e){
    console.log("checkAcceptRequest: ", e)
  }
  
};
export const checkWithdrawRequest = async (
  req: express.Request,
  res: express.Response
) => {
  try{
  let withdrewRequestArr: any[] = [];
  if (req.session) {
    const data = await knex.select(["visitor_product_id", "id"]).from("exchange_product_proposal").where("target_product_user_id", req.session.user.id).andWhere("withdraw", true)

    for (let i = 0; i < data.length; i++) {
      const fetchedData = await knex.select(["product.id",
        "product.product_name",
        "product_image.product_image"]).from("product").innerJoin("product_image","product.id","product_image.product_id ").where("product.id", data[i].visitor_product_id)
      withdrewRequestArr.push([fetchedData[0], data[i].id]);
    }
  }
  res.json(withdrewRequestArr);
  }catch(e){
      
  }
};
export const checkRejectRequest = async (
  req: express.Request,
  res: express.Response
) => {
  let rejectedRequestArr: any[] = [];
  if (req.session) {
    const data = await knex.select(["target_product_id", "id"]).from("exchange_product_proposal").where("user_id", req.session.user.id).andWhere("reject", true)

    for (let i = 0; i < data.length; i++) {
      const fetchedData = await knex.select(["product.id",
        "product.product_name",
        "product_image.product_image"]).from("product").innerJoin("product_image","product.id","product_image.product_id ").where("product.id", data[i].target_product_id)
      rejectedRequestArr.push([fetchedData[0], data[i].id]);
    }
  }
  res.json(rejectedRequestArr);
};

export const confirmRejectAndWithdaw = async (
  req: express.Request,
  res: express.Response
) => {
  await knex("exchange_product_proposal").where("id", req.params.request_id).del()
  res.redirect("/");
};
export const getUserName = async (
  req: express.Request,
  res: express.Response
) => {
  const usersId = await knex.select(["user_id","target_product_user_id"]).from("exchange_product_proposal").where("id",req.params.request_id )
  const visitor_name = await knex.select("username").from("users").where("id",usersId[0].user_id)
  const target_user_name = await knex.select("username").from("users").where("id",usersId[0].target_product_user_id)
  res.json({ visitor_name, target_user_name, usersId });
};
export const completedRequest = async (
  req: express.Request,
  res: express.Response
) => {
  try {
    const timeSlotData = await knex("suggested_time").where("id", req.params.time_slot_id).del().returning("user_id").returning(["content", "date", "time_from", "time_to", "location"])
    await knex("suggested_time").where("exchange_product_proposal_id", req.params.request_id).del()
      const requestDetail = await knex("exchange_product_proposal").where("id", req.params.request_id).del().returning("user_id")
    // --------------------------
    await knex("exchange_product_proposal").where("target_product_id", req.params.target_product_id).del()
    await knex("exchange_product_proposal").where("target_product_id", req.params.visitor_product_id).del()
    await knex("exchange_product_proposal").where("visitor_product_id", req.params.target_product_id).del()
    await knex("exchange_product_proposal").where("visitor_product_id", req.params.visitor_product_id).del()
    // ------------------------
    await knex("like").where("product_id", req.params.target_product_id).del()
    await knex("like").where("product_id", req.params.visitor_product_id).del()
    await knex("comment").where("product_id", req.params.target_product_id).del()
     await knex("comment").where("product_id", req.params.visitor_product_id).del()
    const targetProductImage = await knex("product_image").where("product_id", req.params.target_product_id).returning("product_image").del()
    const visitorProductImage = await knex("product_image").where("product_id", req.params.visitor_product_id).returning("product_image").del()
 
    const targetProductName = await knex("product").where("id", req.params.target_product_id).returning("product_name").del()
    const visitorProductName = await knex("product").where("id", req.params.visitor_product_id).del().returning("product_name")
    await knex.insert({target_user_id:req.params.target_user_id, visitor_user_id: requestDetail[0], confirm_from_target_user: false, confirm_from_requester: false, target_product_name: targetProductName[0], target_product_image: targetProductImage[0], visitor_product_name: visitorProductName[0], visitor_product_image: visitorProductImage[0], content:timeSlotData[0].content, date: timeSlotData[0].date, time_from: timeSlotData[0].time_from, time_to: timeSlotData[0].time_to, location: timeSlotData[0].location}).into("transaction_record")
    res.redirect("/");
  } catch (e) {
    console.log("completedRequest: ", e);
  }
};
export const checkCompletedTransaction = async (
  req: express.Request,
  res: express.Response
) => {
  try {
    if (req.session) {
      const targetUser = await knex.select("*").from("transaction_record").where("target_user_id", req.session.user.id)
      const requester = await knex.select("*").from("transaction_record").where("visitor_user_id", req.session.user.id)
      res.json({ targetUser, requester });
    } else {
      res.json({ result: "none" });
    }
  } catch (e) {
    console.log("checkCompletedTransaction: ", e);
  }
};

export const toConfirmRequestFromRequester = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    await knex("transaction_record").update({confirm_from_requester:true}).where("id", req.params.record_id)
    res.redirect("/");
  }catch(e){
    console.log("toConfirmRequestFromRequester: ", e)
  }
  
};
export const toConfirmRequestFromTargetUser = async (
  req: express.Request,
  res: express.Response
) => {
  try{
    await knex("transaction_record").update({confirm_from_target_user:true}).where("id", req.params.record_id)
    res.redirect("/");
  }catch(e){
    console.log("toConfirmRequestFromTargetUser: ", e)
  }
  
};
exchangeRoute.get("/check-user-name/:request_id", getUserName);
exchangeRoute.get("/to-exchange", toExchange);
exchangeRoute.post(
  "/request/:visit_product_id/:target_product_id/:target_product_user_id",
  choseWhatToExchange
);
exchangeRoute.put(
  "/accept-time-slot/:time_slot_id/:visitor_product_id/:target_product_id/:request_id/:target_product_user_id",
  acceptTimeSlot
);
exchangeRoute.put(
  "/cancel-time-slot/:time_slot_id/:visitor_product_id/:target_product_id/:request_id/:target_product_user_id",
  cancelTimeSlot
);
exchangeRoute.get("/check-withdraw-request", checkWithdrawRequest);
exchangeRoute.get("/check-reject-request", checkRejectRequest);
exchangeRoute.get("/check-accept-request/:request_id", checkAcceptRequest);
exchangeRoute.get("/receive", receiveRequest);
exchangeRoute.put(
  "/accept/:request_id/:target_product_id/:visitor_product_id",
  requestAccepted
);
exchangeRoute.get("/transaction", showConfirmTransaction);
exchangeRoute.get("/time-slot/:request_id", getTimeSlot);
exchangeRoute.delete(
  "/time-slot/delete/:time_slot_id/:target_product_id/:visitor_product_id/:request_id/:user_id",
  deleteTimeSlot
);
exchangeRoute.delete(
  "/confirm-reject-withdraw/:request_id",
  confirmRejectAndWithdaw
);
exchangeRoute.put("/reject/:request_id/:requester_id", rejectRequest);
exchangeRoute.put("/withdraw/:request_id/:requester_id", withdrawRequest);
exchangeRoute.post(
  "/time_adding/:request_id/:target_product_id/:visitor_product_id",
  storeTime
);
exchangeRoute.post(
  "/complete/:time_slot_id/:visitor_product_id/:target_product_id/:request_id/:target_user_id",
  completedRequest
);
exchangeRoute.get("/check-confirm", checkCompletedTransaction);
exchangeRoute.put(
  "/to-confirm-complete-request-from-request/:record_id",
  toConfirmRequestFromRequester
);
exchangeRoute.put(
  "/to-confirm-complete-request-from-target/:record_id",
  toConfirmRequestFromTargetUser
);
