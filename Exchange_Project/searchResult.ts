import express from "express";
import { knex } from "./server";
export const searchResultRoute = express.Router();

export const getProducts = async (
  req: express.Request,
  res: express.Response
) => {
  let likeArr: any[] = [];
  let userArr: any[] = [];
  const products = await knex.select(["product.id",
    "product.product_name",
    "product.created_at",
    "product.location",
    "product.wished_goods_to_exchange",
    "product.user_id",
    "product_image.product_image"]).from("product").innerJoin("product_image", "product.id", "product_image.product_id").where("product.category_id", req.params.cate).andWhereNot("product.accepted_for_exchange", true)


  let groupedProducts = new Map();
  for (let item of products) {
    const image = {
      image: item.product_image,
    };
    if (groupedProducts.has(item.id)) {
      groupedProducts.get(item.id).images.push(image);
    } else {
      const product = {
        product_id: item.id,
        product_name: item.product_name,
        created_at: item.created_at,
        user_id: item.user_id,
        wishedForExchange: item.wished_goods_to_exchange,
        location: item.location,
        images: [image],
      };
      groupedProducts.set(item.id, product);
    }
  }
  const result = Array.from(groupedProducts.values());
  for (let i = 0; i < result.length; i++) {
    const likes = await knex.select(["id", "product_id"]).from("like").where("product_id", result[i].product_id)
    const user = await knex.select(["id", "username", "profilepicture"]).from("users").where("id", result[i].user_id)
    userArr.push(user);
    likeArr.push(likes);
  }

  res.json({ result, userArr, likeArr });
};

export const getProductsBySearch = async (
  req: express.Request,
  res: express.Response
) => {
  let likeArr: any[] = [];
  let userArr: any[] = [];
  const products = await knex.select(["product.id",
  "product.product_name",
  "product.created_at",
  "product.location",
  "product.wished_goods_to_exchange",
  "product.user_id",
  "product_image.product_image"]).from("product").innerJoin("product_image", "product.id", "product_image.product_id").whereRaw(`LOWER(product.product_name) LIKE ?`, [`%${req.params.term.toLowerCase()}%`]).andWhereNot("product.accepted_for_exchange", true)

  let groupedProducts = new Map();
  for (let item of products) {
    const image = {
      image: item.product_image,
    };
    if (groupedProducts.has(item.id)) {
      groupedProducts.get(item.id).images.push(image);
    } else {
      const product = {
        product_id: item.id,
        product_name: item.product_name,
        created_at: item.created_at,
        user_id: item.user_id,
        wishedForExchange: item.wished_goods_to_exchange,
        location: item.location,
        images: [image],
      };
      groupedProducts.set(item.id, product);
    }
  }
  const result = Array.from(groupedProducts.values());
  for (let i = 0; i < result.length; i++) {
    const likes = await knex.select(["id", "product_id"]).from("like").where("product_id", result[i].product_id)
    const user = await knex.select(["id", "username", "profilepicture"]).from("users").where("id", result[i].user_id)
    userArr.push(user);
    likeArr.push(likes);
  }

  res.json({ result, userArr, likeArr });
};

export const searchTerm = (req: express.Request, res: express.Response) => {
  res.redirect("/search?term=" + req.body.term);
};

export const getAllProducts = async (
  req: express.Request,
  res: express.Response
) => {
  let likeArr: any[] = [];
  let userArr: any[] = [];
  const products = await knex.select(["product.id",
  "product.product_name",
  "product.created_at",
  "product.location",
  "product.wished_goods_to_exchange",
  "product.user_id",
  "product_image.product_image"]).from("product").innerJoin("product_image", "product.id", "product_image.product_id").andWhereNot("product.accepted_for_exchange", true)
  let groupedProducts = new Map();
  for (let item of products) {
    const image = {
      image: item.product_image,
    };
    if (groupedProducts.has(item.id)) {
      groupedProducts.get(item.id).images.push(image);
    } else {
      const product = {
        product_id: item.id,
        product_name: item.product_name,
        created_at: item.created_at,
        user_id: item.user_id,
        wishedForExchange: item.wished_goods_to_exchange,
        location: item.location,
        images: [image],
      };
      groupedProducts.set(item.id, product);
    }
  }
  const result = Array.from(groupedProducts.values());
  for (let i = 0; i < result.length; i++) {
    const likes = await knex.select(["id", "product_id"]).from("like").where("product_id", result[i].product_id)
    const user = await knex.select(["id", "username", "profilepicture"]).from("users").where("id", result[i].user_id)
    userArr.push(user);
    likeArr.push(likes);
  }

  res.json({ result, userArr, likeArr });
};



searchResultRoute.get("/detail/all-product", getAllProducts);
searchResultRoute.get("/detail/by-search/:term", getProductsBySearch);
searchResultRoute.post("/detail/by-search", searchTerm);
searchResultRoute.get("/detail/:cate", getProducts);
