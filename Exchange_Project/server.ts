import express from "express"
import dotenv from "dotenv"
import bodyParser from "body-parser"
import multer from "multer"
import Knex from 'knex';
import expressSession from "express-session"
import { isLoggedIn } from "./guard"
import methodOverride from "method-override"
import knexConfigs = require('./knexfile');
import fs from "fs";
dotenv.config()
const app = express()

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static("public"));
app.use(express.static('uploads'));
app.use(methodOverride('_method'))
import grant from 'grant-express';
let mode = process.env.NODE_ENV || "test";
let envFile = ".env." + mode; 
let envFileContent = fs.readFileSync(envFile).toString();
let env = dotenv.parse(envFileContent)
let knexConfig = knexConfigs[mode]
export const knex = Knex(knexConfig)
app.use(
  expressSession({
    secret: "Whatever you like la.",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(grant({
  "defaults":{
      "protocol": "https",
      "host": "exchangehk.tk",
      "transport": "session",
      "state": true,
  },
  "google":{
      "key": env.GOOGLE_CLIENT_ID || "",
      "secret": env.GOOGLE_CLIENT_SECRET || "",
      "scope": ["profile","email"],
      "callback": "/login/google"
    },
}));

declare module "express-session" {
  interface Session {
    user: {
      id:number
    };
  }
}
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `${__dirname}/uploads`);
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
})
export const upload = multer({storage: storage})




import { loginRoute } from "./loginRoute"
import {registerRoute} from "./registerRoute"
import { profileDetailRoute } from "./profileDetailPage"
import { mainPageRoute } from "./mainPageRoute"
import { exchangeRoute } from "./exchangeRoute"
import {searchResultRoute} from "./searchResult"
import { transactionRecordRoute } from "./transactionRecordRoute"
app.use('/', mainPageRoute);
app.use('/', loginRoute);
app.use('/register', registerRoute);
app.use('/search', searchResultRoute);
app.use('/profile', isLoggedIn, profileDetailRoute);
app.use('/exchange',isLoggedIn, exchangeRoute);
app.use('/record',isLoggedIn, transactionRecordRoute);



app.use(isLoggedIn,express.static('protected/profile'));
app.use(isLoggedIn,express.static('protected/productUpload'));
app.use(isLoggedIn,express.static('protected/productDetail'));
app.use(isLoggedIn,express.static('protected/exchange-process/transactionRecord'));
app.use(isLoggedIn,express.static('protected/exchange-process/receiveRequest'));
app.use(isLoggedIn,express.static('protected/exchange-process/toExchange'));
app.use(isLoggedIn,express.static('protected/editProfile'));

app.listen(8080, () => {
  console.log(`Server is running on at http://127.0.0.1:${8080}`)
})