CREATE DATABASE exchange;

CREATE TABLE "users" (
    id SERIAL PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    profilePicture VARCHAR(255),
    is_google BOOLEAN,
    createdTime TIMESTAMP
);


CREATE TABLE "category"(
    id SERIAL primary key,
    name VARCHAR(255)
);

CREATE TABLE "product"(
    id SERIAL primary key,
    category_id integer NOT NULL,
    FOREIGN KEY (category_id) REFERENCES category(id),
    user_id integer NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id),
    product_name VARCHAR(255) NOT NULL,
    created_time TIMESTAMP,
    location VARCHAR(255) NOT NULL,
    description TEXT NOT NULL,
    View INTEGER NOT NULL,
    Wished_Goods_To_Exchange TEXT
);

CREATE TABLE "comment"(
    id SERIAL primary key,
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id),
    product_id integer,
    FOREIGN KEY (product_id) REFERENCES product(id),
    content TEXT,
    created_time TIMESTAMP
);

CREATE TABLE "rating"(
    id SERIAL primary key,
    target_user_id integer,
    FOREIGN KEY (target_user_id) REFERENCES users(id),
    visitor_id integer,
    FOREIGN KEY (visitor_id) REFERENCES users(id),
    rating integer
);



CREATE TABLE "product_image"(
    id SERIAL primary key,
    product_id integer,
    FOREIGN KEY (product_id) REFERENCES product(id),
    product_image VARCHAR(255)
);



CREATE TABLE "like"(
    id SERIAL primary key,
    user_id integer NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id),
    product_id integer,
    FOREIGN KEY (product_id) REFERENCES product(id)
);

CREATE TABLE "exchange_product_proposal"(
    id SERIAL primary key,
    target_product_id integer,
    FOREIGN KEY (target_product_id) REFERENCES product(id),
    user_id integer NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id),
    visitor_product_id integer,
    FOREIGN KEY (visitor_product_id) REFERENCES product(id),
    target_product_user_id integer,
    FOREIGN KEY (target_product_user_id) REFERENCES users(id),
    reject BOOLEAN,
    accept BOOLEAN,
    withdraw BOOLEAN
);

CREATE TABLE "suggested_time"(
    id SERIAL primary key,
    exchange_product_proposal_id integer,
    FOREIGN KEY (exchange_product_proposal_id) REFERENCES exchange_product_proposal(id),
    from_target_user VARCHAR(255),
    from_requester VARCHAR(255),
    content VARCHAR(255),
    date VARCHAR(255),
    time_from VARCHAR(255),
    time_to VARCHAR(255),
    location VARCHAR(255),
    accept BOOLEAN
);

CREATE TABLE "transaction_record"(
    id SERIAL primary key,
    target_user_id integer NOT NULL,
    FOREIGN KEY (target_user_id) REFERENCES users(id),
    visitor_user_id integer NOT NULL,
    FOREIGN KEY (visitor_user_id) REFERENCES users(id),
    confirm_from_target_user BOOLEAN,
    confirm_from_requester BOOLEAN,
    target_product_name VARCHAR(255) NOT NULL,
    target_product_image VARCHAR(255),
    visitor_product_name VARCHAR(255) NOT NULL,
    visitor_product_image VARCHAR(255),
    content VARCHAR(255),
    date VARCHAR(255) NOT NULL,
    time_from VARCHAR(255),
    time_to VARCHAR(255),
    location VARCHAR(255) NOT NULL
);


INSERT INTO "category" (name) VALUES ('home_and_furniture'), ('electroncs'), ('women_fashion'), ('men_fashion'), ('toy_and_games'), ('kids_clothes'), ('kitchen_and_appliances'), ('books'), ('sports'), ('others');
