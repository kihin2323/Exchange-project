import express from "express";
import path from "path";
import fetch from "node-fetch";
import {knex} from './server'
import { checkPassword } from "./hash";
import { hashPassword } from "./hash";
export const loginRoute = express.Router();

export const getLogin = (req: express.Request, res: express.Response) => {
  res.sendFile(path.resolve(__dirname, "./public/login.html"));
};

export const toLogin = async (req: express.Request, res: express.Response) => {
  try{
    const { email, password } = req.body;
    const checkEmail = await knex.select("*").from("users").where("email", email)
  if (checkEmail.length !== 1) {
    return res
      .status(401)
      .redirect("/login.html?error=Please+enter+correct+email+and+password");
  }
  const hashedPassword = checkEmail[0].password;
  const match = await checkPassword(password, hashedPassword);
  if (!match) {
    return res
      .status(401)
      .redirect("/login.html?error=Please+enter+correct+email+and+password");
  }
  if (req.session) {
    req.session.user = {
      id: checkEmail[0].id,
    };
    res.redirect("/");
  } else {
    res.redirect("/");
  }
  }catch(e){
    console.log("toLogin: ", e)
  }
};

export const getLogout = (
  req: express.Request,
  res: express.Response
) => {
  if (req.session) {
    delete req.session.user;
  }
  res.redirect("/login.html");
};
export const loginGoogle = async (
  req: express.Request,
  res: express.Response
) => {
  try{

  
  const accessToken = req.session?.grant.response.access_token;
  const fetchRes = await fetch(
    "https://www.googleapis.com/oauth2/v2/userinfo",
    {
      method: "get",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }
  );
  const result = await fetchRes.json();
 
  const users = await knex.select("*").where("email", result.email).from("users")

  const user = users[0];
  if (!user) {
    const { name, picture, email } = result;
    const hashedPassword = await hashPassword(makeId(10));
    await knex.insert({username: name, password:hashedPassword, profilepicture:picture, email,  is_google: true}).into("users")
    const fetchedData = await knex.select("*").from("users").where("email", email)
    if (req.session) {
      req.session.user = {
        id: fetchedData[0].id,
      };
    }
  } else {
    if (req.session) {
      req.session.user = {
        id: user.id,
      };
    }
  }
  return res.redirect("/");
   }catch(e){
    console.log("loginGoogle: ", e)
   }
};

function makeId(length) {
  let result = "";
  let characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

loginRoute.get("/login", getLogin);
loginRoute.post("/login", toLogin);
loginRoute.get("/login/google", loginGoogle);
loginRoute.get("/logout", getLogout);
