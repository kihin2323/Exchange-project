import express from 'express';


export const isLoggedIn = (req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.session && req.session.user != null) {
        next();
    } else {
        res.redirect("/login")
    }
}
