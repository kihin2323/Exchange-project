import express from "express";
import { upload, knex } from "./server";
import path from "path";
import { hashPassword, checkPassword } from "./hash";

export const profileDetailRoute = express.Router();

export const getDetail = async (
  req: express.Request,
  res: express.Response
) => {
  if (req.session && req.session.user) {
    const fetchedData = await knex.select(["id", "username", "profilepicture", "created_at", "email", "is_google"]).from("users").where("id", req.session.user.id)
    res.json(fetchedData);
  }
};

export const profileDetail = (req: express.Request, res: express.Response) => {
  res.sendFile(path.resolve(__dirname, "protected/profile/profile.html"));
};
export const profileDetailUpdate = (
  req: express.Request,
  res: express.Response
) => {
  res.sendFile(
    path.resolve(__dirname, "protected/editProfile/editProfile.html")
  );
};

export const profileProductUpload = (
  req: express.Request,
  res: express.Response
) => {
  res.sendFile(
    path.resolve(__dirname, "protected/productUpload/productUpload.html")
  );
};
export const profileProductUploaded = async (
  req: express.Request,
  res: express.Response
) => {
  const { name, description, wishedProduct, category, location } = req.body;
  if (req.session) {
    const result: any = await knex.insert({ category_id: category, user_id: req.session.user.id, product_name: name.trim(), location: location.trim(), description: description.trim(), view: 0, wished_goods_to_exchange: wishedProduct || null,accepted_for_exchange: false }).into("product").returning("id")
    const productId = result[0];
    for (let i = 0; i < req.files.length; i++) {
      await knex.insert({ product_id: productId, product_image: req.files[i].filename }).into("product_image")
    }
    if (req.session) {
      res.redirect("/profile?id=" + req.session.user.id);
    }
  };
}
export const deleteProduct = async (
  req: express.Request,
  res: express.Response
) => {
  const checkIfInRequest = await knex.select("id").from("exchange_product_proposal").where("target_product_user_id", req.params.id)
  const checkIfInRequestForExchange = await knex.select("id").from("exchange_product_proposal").where("visitor_product_id", req.params.id)
  if (
    checkIfInRequest.length === 0 &&
    checkIfInRequestForExchange.length === 0
  ) {
    await knex("like").where("product_id", req.params.id).del()
    await knex("comment").where("product_id", req.params.id).del()
    await knex("product_image").where("product_id", req.params.id).del()
    await knex("product").where("id", req.params.id).del()
    if (req.session) {
      res.redirect("/profile?id=" + req.session.user.id);
    }
  } else if (req.session) {
    res
      .status(401)
      .redirect(
        "/profile?id=" +
        req.session.user.id +
        "&error=This+Product+is+set+for+exchange+already,+Please+reject+or+withdraw+all+requests+before+deleting."
      );
  }
};

export const getProfileDetail = async (
  req: express.Request,
  res: express.Response
) => {
  const resultData = await knex.select(["id", "username", "profilepicture", "created_at"]).from("users").where("id", req.params.id)
  const getRating = await knex.select("*").from("rating").where("target_user_id", req.params.id)
  const combined = {
    resultData,
    getRating,
  };
  res.json(combined);
};

export const storeRating = async (
  req: express.Request,
  res: express.Response
) => {
  await knex.insert({ target_user_id: req.params.targetId, visitor_id: req.params.visitorId, rating: req.body.rating }).into("rating")
  res.redirect("/profile?id=" + req.params.targetId);
};

export const validPass = (ChangePassword) => {
  const validPassword =
    typeof ChangePassword === "string" &&
    ChangePassword.trim() !== "" &&
    ChangePassword.trim().length >= 6;
  return validPassword;
};
export const editProfile = async (
  req: express.Request,
  res: express.Response
) => {
  const { changeUsername, ChangePassword, confirmChangePassword } = req.body;

  if (ChangePassword !== confirmChangePassword) {
    return res.redirect("/editProfile.html?error=Passwords+must+match");
  }
  if (req.session) {
    const originData = await knex.select(["username", "password", "profilepicture"]).from("users").where("id", req.session.user.id).first()


    const checkOldPassword = await checkPassword(
      req.body.oldPassword,
      originData.password
    );
    if (!checkOldPassword && req.body.oldPassword !== "") {
      return res.redirect("/editProfile.html?error=Passwords+must+match");
    }
    if (validPass(ChangePassword)) {
      const hashedPassword = await hashPassword(ChangePassword);
      await knex("users").update({ username: changeUsername || originData.username, password: hashedPassword || originData.password, profilepicture: req.file?.filename || originData.profilepicture }).where("id", req.session.user.id)
    } else {
      await knex("users").update({ username: changeUsername || originData.username, profilepicture: req.file?.filename || originData.profilepicture }).where("id", req.session.user.id)
    }
  }
  if (req.session) {
    res.redirect("/profile?id=" + req.session.user.id);
  }
};

export const showProductDetail = (
  req: express.Request,
  res: express.Response
) => {
  res.sendFile(
    path.resolve(__dirname, "./protected/productDetail/productDetail.html")
  );
};
profileDetailRoute.get("/detail", showProductDetail);
profileDetailRoute.post("/give-rating/:targetId/:visitorId", storeRating);
profileDetailRoute.get("/user-detail", getDetail);
profileDetailRoute.delete("/product/delete/:id", deleteProduct);
profileDetailRoute.get("/update", profileDetailUpdate);
profileDetailRoute.put("/editProfile", upload.single("image"), editProfile);
profileDetailRoute.get("/product/upload", profileProductUpload);
profileDetailRoute.post(
  "/product/upload",
  upload.array("image"),
  profileProductUploaded
);
profileDetailRoute.get("/user-detail/:id", getProfileDetail);
profileDetailRoute.get("/", profileDetail);
